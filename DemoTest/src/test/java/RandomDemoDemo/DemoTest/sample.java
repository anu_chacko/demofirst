package RandomDemoDemo.DemoTest;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class sample {
	public static WebDriver driver;
	@Test
	public void test() throws InterruptedException
	{
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--remote-allow-origins=*");
		driver=new ChromeDriver(options);
		driver.get("https://www.google.com/");
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//a[@aria-label='Google apps']")).click();
	
		int size=driver.findElements(By.tagName("iframe")).size();
		System.out.println(size);
		driver.switchTo().frame("app");
		List<WebElement> c= driver.findElements(By.xpath("//li[@class='j1ei8c']"));
		c.size();
		System.out.println(c.size());
		
	}
}
